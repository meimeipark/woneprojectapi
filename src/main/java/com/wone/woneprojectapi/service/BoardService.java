package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Board;
import com.wone.woneprojectapi.enums.BoardType;
import com.wone.woneprojectapi.enums.ResultCode;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class BoardService {
    private final BoardRepository boardRepository;

    /**
     * 공지사항 게시판 등록
     * @param request BoardType(공지사항, 이용방법), 작성시간(현재시간:자동), 제목, 내용
     */
    public void setNoticeBoard (BoardCreateRequest request){
        Board addData = new Board();
        addData.setBoardType(BoardType.NOTICE);
        addData.setBoardCreateDate(LocalDateTime.now());
        addData.setBoardTitle(request.getBoardTitle());
        addData.setBoardContent(request.getBoardContent());

        boardRepository.save(addData);
    }
    /**
     * 이용방법 게시판 등록
     * @param request BoardType(공지사항, 이용방법), 작성시간(현재시간:자동), 제목, 내용
     */
    public void setGuideBoard (BoardCreateRequest request){
        Board addData = new Board();
        addData.setBoardType(BoardType.GUIDE);
        addData.setBoardCreateDate(LocalDateTime.now());
        addData.setBoardTitle(request.getBoardTitle());
        addData.setBoardContent(request.getBoardContent());

        boardRepository.save(addData);
    }

    /**
     * 게시판 공지사항만 모두 조회
     * @return BoardType.NOTICE만 조회
     */

    /** BoardType 별로 조회하기 **/
    public List<BoardNoticeItem> getNotices(BoardType boardType){
        List<Board> originList = boardRepository.findAllByBoardTypeOrderByIdDesc(boardType);

        List<BoardNoticeItem> result = new LinkedList<>();

        for(Board board: originList){
            BoardNoticeItem addItem = new BoardNoticeItem();
            addItem.setId(board.getId());
            addItem.setBoardType(board.getBoardType().getBoardType());
            addItem.setBoardCreateDate(board.getBoardCreateDate().toLocalDate());
            addItem.setBoardTitle(board.getBoardTitle());
            addItem.setBoardContent(board.getBoardContent());

            result.add(addItem);
        }
        return result;
    }


    /**
     * 게시판 이용방법만 모두 조회
     * @return BoardType.GUIDE만 조회
     */
    // 이용방법 조회
    public List<BoardUseItem> getUseBoards(){
        List<Board> originList = boardRepository.findAllByBoardTypeEqualsOrderByIdDesc(BoardType.GUIDE);

        List<BoardUseItem> result = new LinkedList<>();
        for(Board board: originList){
            BoardUseItem addItem = new BoardUseItem();
            addItem.setId(board.getId());
            addItem.setBoardType(board.getBoardType().getBoardType());
            addItem.setBoardTitle(board.getBoardTitle());
            addItem.setBoardContent(board.getBoardContent());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 게시판(공지사항) 내용 상세보기
     * @param id 게시판 id를 받아
     * @return 해당 글 상세 보기
     */

    // 게시판 상세보기
    public BoardNoticeResponse getNoticeBoard(long  id){

        Board originData = boardRepository.findById(id).orElseThrow();

        BoardNoticeResponse response = new BoardNoticeResponse();
        response.setId(originData.getId());
        response.setBoardType(originData.getBoardType());
        response.setBoardCreateDate(originData.getBoardCreateDate().toLocalDate());
        response.setBoardTitle(originData.getBoardTitle());
        response.setBoardContent(originData.getBoardContent());

        return response;
    }

    /**
     * 이용방법 상세보기 (등록날짜 제거 ver)
     * @param id
     * @return
     */
    // 이용방법 상세보기
    public BoardUseResponse getUseBoard(long  id){
        Board originData = boardRepository.findById(id).orElseThrow();

        BoardUseResponse response = new BoardUseResponse();
        response.setId(originData.getId());
        response.setBoardType(originData.getBoardType());
        response.setBoardTitle(originData.getBoardTitle());
        response.setBoardContent(originData.getBoardContent());

        return response;
    }

    /**
     * 게시판 페이징
     */
    public ListResult<BoardItem> getGuides(int pageNum, BoardType boardType) {
        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<Board> boards = boardRepository.findALLByBoardTypeOrderByIdDesc(pageRequest, boardType);

        List<BoardItem> result = new LinkedList<>();
        for (Board board : boards.getContent()) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setBoardType(board.getBoardType().getBoardType());
            addItem.setBoardCreateDate(board.getBoardCreateDate().toLocalDate());
            addItem.setBoardTitle(board.getBoardTitle());
            addItem.setBoardContent(board.getBoardContent());

            result.add(addItem);
        }

        ListResult<BoardItem> response = new ListResult<>();
        response.setList(result);
        response.setMsg(ResultCode.SUCCESS.getMsg());
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setTotalCount(boards.getTotalElements());
        response.setTotalPage(boards.getTotalPages());
        response.setCurrentPage(boards.getPageable().getPageNumber() +1 );

        return response;
    }


    /**
     * 게시판 내용 수정하기
     * @param id
     * @param request 게시글 id를 가져와서 타입, 제목, 내용 수정
     */
    // 게시판 수정
    public void putBoard(long id, BoardContentChangeRequest request){
        Board originData = boardRepository.findById(id).orElseThrow();

        originData.setBoardType(request.getBoardType());
        originData.setBoardTitle(request.getBoardTitle());
        originData.setBoardContent(request.getBoardContent());

        boardRepository.save(originData);
    }

    /**
     * 게시글 삭제하기
     * @param id 게시글 id를 선택하여 삭제
     */
    // 게시판 삭제
    public void delBoard(long id){
        boardRepository.deleteById(id);
    }
}
