package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.LoginRequest;
import com.wone.woneprojectapi.model.LoginResponse;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;

    //프리픽스 do > 무언가의 행위
    public SingleResult<LoginResponse> doLogin(LoginRequest request) {
        // 아이디와 비밀번호가 일치하는 데이터를 가져온다.
        // Optional 있을수도 있고 없을수도 있다.
        Optional<Member> member = memberRepository.findByUserIdAndPassword(
                request.getUserId(),
                request.getPassword()
        );

        // 회원데이터는 있을수도 있고... 없을수도 있어...
        // 만약에 회원데이터가 있으면...LoginResponse 채워서 준다...
        if (member.isPresent()) {
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setMemberId(member.get().getId());
            loginResponse.setMemberName(member.get().getUserId());

            SingleResult<LoginResponse> result = new SingleResult<>();
            result.setData(loginResponse);
            result.setCode(0);
            result.setMsg("로그인 성공");

            return result;
        } else { // 회원 데이터 없으면...
            SingleResult<LoginResponse> result = new SingleResult<>();
            result.setCode(-1);
            result.setMsg("아이디나 패스워드를 확인해 주세요.");

            return result;
        }
        // 근데 없으면..? 메세지 예쁘게 줘야하니까.. 던지면 안된다!
    }
}
