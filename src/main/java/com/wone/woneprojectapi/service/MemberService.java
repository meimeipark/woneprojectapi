package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.enums.MemberGroup;
import com.wone.woneprojectapi.enums.MemberStatus;
import com.wone.woneprojectapi.enums.ResultCode;
import com.wone.woneprojectapi.lib.CommonFile;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.NearDayStatisticsResponse;
import com.wone.woneprojectapi.model.member.*;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MemberService {
    private final MemberRepository memberRepository;

    // csv 업로드
    public void setMemberListByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // Parsing
        // 버퍼에서 넘어온 string 한줄을 임시로 담아둘 변수가 필요함
        String line = "";

        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1씩 증가해서 기록 해 줄 변수가 필요함
        int index = 0;

        // 얼마나 받을지 알 수 없으므로 while 반복문
        // null 이 되기 전까지 반복
        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                String[] cols = line.split(","); // ,기준으로 쪼개 줘
                if (cols.length == 7) {
                    System.out.println(cols[0]);
                    System.out.println(cols[1]);
                    System.out.println(cols[2]);
                    System.out.println(cols[3]);
                    System.out.println(cols[4]);
                    System.out.println(cols[5]);
                    System.out.println(cols[6]);

                    // DB에 넣기
                    Member addData = new Member();
                    addData.setUserId(cols[0]);
                    addData.setUsername(cols[1]);
                    addData.setBirthDate(LocalDate.parse(cols[2]));
                    addData.setPassword(cols[3]);
                    addData.setJoinDate(LocalDateTime.parse(cols[4]));
                    addData.setMemberGroup(MemberGroup.valueOf(cols[5]));
                    addData.setMemberStatus(MemberStatus.valueOf(cols[6]));

                    memberRepository.save(addData);
                }
            }
            index++;
        }
        bufferedReader.close();
    }

    /**
     *
     * @param id memberId를 받아
     * @return 조인
     */
    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }


    /**
     * 회원 가입 시킨
     *
     * @param request 회원 가입 창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치 하지 않을 경우
     *
     */
    // 회원 가입
    public void setMemberJoin(MemberJoinRequest request) throws Exception {
        if (!isNewUserId(request.getUserId())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();
        Member member = new Member();
        member.setUserId(request.getUserId());
        member.setUsername(request.getUsername());
        member.setBirthDate(request.getBirthDate());
        member.setPassword(request.getPassword());
        member.setJoinDate(LocalDateTime.now());
        member.setMemberGroup(MemberGroup.USER_MEMBER);
        member.setMemberStatus(MemberStatus.NORMAL);

        memberRepository.save(member);
    }


    /**
     * 신규 아이디인지 확인한다.
     * @param userId 아이디
     * @return true 신규아이디 / false 중복 아이디
     */
    // id 중복확인을 참거짓으로 표현해라(id를 받아)
    // 중복검사를 했을 때 0보다 크면 돌려줘라
    private boolean isNewUserId (String userId) {
        long dupCount = memberRepository.countByUserId(userId);

        return dupCount <= 0;
    }

    // 중복확인
    public MemberDupCheckResponse getMemberIdDupCheck(String userId){
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUserId(userId));

        return response;
    }


    /**
     *
     * @return 전체 회원 조회
     */
    // 전체 회원 조회
    // 관리자는 처음에 MemberGroupType 변경 필요
    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member: originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setUserId(member.getUserId());
            addItem.setUsername(member.getUsername());
            addItem.setJoinDate(member.getJoinDate().toLocalDate());
            addItem.setMemberGroupType(member.getMemberGroup().getPartType());

            result.add(addItem);
        }
        return result;
    }


    /**
     *
     * @param id 를 받아서
     * @return 회원 상세 조회(단수)
     */
    // 회원 정보 회원 ID별로 조회(단수)
    // WEB 관리자 화면 상세 보기
    public MemberResponse getMemberAdmin(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setUserId(originData.getUserId());
        response.setUsername(originData.getUsername());
        response.setBirthDate(originData.getBirthDate());
        response.setPassword(originData.getPassword());
        response.setJoinDate(originData.getJoinDate().toLocalDate());
        response.setMemberGroup(originData.getMemberGroup().getPartType());
        response.setMemberStatus(originData.getMemberStatus().getStatusType());

        return response;
    }

    /**
     *
     * @param id 를 받아서
     * @return 회원 상세 조회(단수)
     */
    // 회원 정보 회원 ID별로 조회(단수)
    // APP 화면 상세 보기
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setUsername(originData.getUsername());
        response.setBirthDate(originData.getBirthDate());
        response.setPassword(originData.getPassword());

        return response;
    }

    // 가입자 수 구하기 (7일 단위)
    public NearDayStatisticsResponse getNearDayStatistics() {

        LocalDateTime today = LocalDateTime.now();
        LocalDateTime startDay = today.minusDays(6);

        NearDayStatisticsResponse response = new NearDayStatisticsResponse();

        List<String> labels = new LinkedList<>();
        List<Long> datasets = new LinkedList<>();

        for (int i=0; i<=6; i++) {

            String LocalDateForm = startDay.plusDays(i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            // 데이터를 2024-02-17T11:45:50.605258718 형식 말고 잘라서 줘야 함. for문 안에 따로 선언

            LocalDateTime startTime = LocalDateTime.of(
                    startDay.plusDays(i).getYear(),
                    startDay.plusDays(i).getMonth(),
                    startDay.plusDays(i).getDayOfMonth(),
                    0,
                    0,
                    0
            );
            LocalDateTime endTime = LocalDateTime.of(
                    startDay.plusDays(i).getYear(),
                    startDay.plusDays(i).getMonth(),
                    startDay.plusDays(i).getDayOfMonth(),
                    23,
                    59,
                    59
            );
            // 비트윈은 시작~끝 지점 사이를 뜻하니 시작, 끝을 만들어 줌 => 시작일 기준 하루 0시 0분 0초 부터 23시 59분 59초까지
            labels.add(LocalDateForm);
            datasets.add(memberRepository.countByJoinDateBetween(startTime, endTime));

            // 레포지토리에서 날짜세기 하는데 시작일로부터 plus i만큼 세기
        }
        // 라벨과 데이트셑에 1일~ 2일~ 3일~ 4일~ 반복 해야하니까 for문으로 돌리기

        response.setLabels(labels);
        response.setDatasets(datasets);

        return response;
    }

    // 페이징
    public ListResult<MemberItem> getMemberPages(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 10, (Sort.by(Sort.Direction.DESC,"id")));
        Page<Member> members = memberRepository.findAll(pageRequest);

        List<MemberItem> result = new LinkedList<>();
        for (Member member : members.getContent()) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setUserId(member.getUserId());
            addItem.setUsername(member.getUsername());
            addItem.setJoinDate(member.getJoinDate().toLocalDate());
            addItem.setMemberGroupType(member.getMemberGroup().getPartType());

            result.add(addItem);
        }

        ListResult<MemberItem> response = new ListResult<>();
        response.setList(result);
        response.setMsg(ResultCode.SUCCESS.getMsg());
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setTotalCount(members.getTotalElements());
        response.setTotalPage(members.getTotalPages());
        response.setCurrentPage(members.getPageable().getPageNumber() + 1);

        return response;
    }

    /**
     *
     * @param id 회원 ID를 받아서
     * @param request username과 birthDate, password를 수정
     */
    // 회원정보 수정
    public void putMemberInfo(long id, MemberInfoChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setUsername(request.getUsername());
        originData.setBirthDate(request.getBirthDate());
        originData.setPassword(request.getPassword());

        memberRepository.save(originData);
    }

    /**
     *
     * @param id 탈퇴할 회원 ID를 받으면
     * @param request Member 상태를 탈퇴(AWAY), 탈퇴날짜(현재날짜)로 변경
     */

    // 회원 탈되(멤버 상태가 탈퇴일 때 로그인이 불가능한 로직 필요)
    public void putMemberOut(long id, MemberOutChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setMemberStatus(MemberStatus.AWAY);
        originData.setOutDate(LocalDateTime.now());

        memberRepository.save(originData);
    }
}
