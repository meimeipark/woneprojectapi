package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.enums.ResultCode;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {
    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());

        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setData(data);

        return result;
    }

    public static <T> ListResult<T> getListResult(List<T> list){
        ListResult<T> result = new ListResult<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setList(list);
        result.setTotalCount((long)list.size());

        return result;
    }
}
