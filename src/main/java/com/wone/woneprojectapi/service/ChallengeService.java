package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.lib.CommonFile;
import com.wone.woneprojectapi.model.challenge.ChallengeCreateRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeGoalsMoneyChangeRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeItem;
import com.wone.woneprojectapi.model.challenge.ChallengeResponse;
import com.wone.woneprojectapi.repository.ChallengeRepository;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ChallengeService {
    private final ChallengeRepository challengeRepository;
    private final MemberRepository memberRepository;

    public Challenge getDataMoney (long id) {
        return challengeRepository.findById(id).orElseThrow();
    }

    /**
     * 목표금액 등록
     * @param member
     * @param request
     */
    public void setChallenge(Member member, ChallengeCreateRequest request){
        Challenge addData = new Challenge();
        addData.setMember(member);
        addData.setChallengeDate(LocalDate.now());
        addData.setGoalsMoney(request.getGoalsMoney());

        challengeRepository.save(addData);
    }


    /**
     * 목표금액 멤버별 조회
     * @param member
     * @return
     */
    // member별 조회
    public List<ChallengeItem> getChallengeMember(Member member){
        List <Challenge> originList = challengeRepository.findAllByMemberOrderByChallengeDateDesc(member);

        List<ChallengeItem> result = new LinkedList<>();
        for(Challenge challenge: originList){
            ChallengeItem addItem = new ChallengeItem();
            addItem.setId(challenge.getId());
            addItem.setMemberId(challenge.getMember().getId());
            addItem.setChallengeDate(challenge.getChallengeDate());
            addItem.setGoalsMoney(challenge.getGoalsMoney());

            result.add(addItem);
        }
        return result;
    }



    // 개별 조회
    public ChallengeResponse getChallenge(long id){
        Challenge originData = challengeRepository.findById(id).orElseThrow();

        ChallengeResponse response = new ChallengeResponse();
        response.setId(originData.getId());
        response.setMemberId(originData.getMember().getId());
        response.setChallengeDate(originData.getChallengeDate());
        response.setGoalsMoney(originData.getGoalsMoney());

        return response;
    }


    // 멤버별 금 도전 금액 조회
    public ChallengeResponse getTodayChallenge(Member member){
        LocalDate today = LocalDate.now();
        Optional<Challenge> originData = challengeRepository.findByMemberAndChallengeDate(member, today);

        ChallengeResponse response = new ChallengeResponse();
        response.setId(originData.get().getId());
        response.setMemberId(originData.get().getMember().getId());
        response.setChallengeDate(originData.get().getChallengeDate());
        response.setGoalsMoney(originData.get().getGoalsMoney());
        return response;
    }

    /**
     * 목표 금액 수정
     * @param id
     * @param request
     */
    public void putChallenge(long id, ChallengeGoalsMoneyChangeRequest request){
        Challenge originData = challengeRepository.findById(id).orElseThrow();
        originData.setGoalsMoney(request.getGoalsMoney());

        challengeRepository.save(originData);
    }
}
