package com.wone.woneprojectapi.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

//@Service
//@RequiredArgsConstructor
//public class TempService {
//    private final RentCarRepository rentCarRepository;
//
//    public void setMemberListByFile(MultipartFile multipartFile) throws IOException {
//        File file = CommonFile.multipartToFile(multipartFile);
//        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
//
//        // Parsing
//        // 버퍼에서 넘어온 string 한줄을 임시로 담아둘 변수가 필요함
//        String line = "";
//
//        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1씩 증가해서 기록 해 줄 변수가 필요함
//        int index = 0;
//
//        // 얼마나 받을지 알 수 없으므로 while 반복문
//        // null 이 되기 전까지 반복
//        while ((line = bufferedReader.readLine()) != null) {
//            if (index > 0) {
//                String[] cols = line.split(","); // ,기준으로 쪼개 줘
//                if (cols.length == 3) {
//                    System.out.println(cols[0]);
//                    System.out.println(cols[1]);
//                    System.out.println(cols[2]);
//
//                    // 이제 DB에 넣기
//                    RentCar addData = new RentCar();
//                    addData.setCarNumber(cols[0]);
//                    addData.setCarName(cols[1]);
//                    addData.setDayPrice(Double.parseDouble(cols[2]));
//
//                    rentCarRepository.save(addData);
//                }
//            }
//            index++;
//        }
//        bufferedReader.close();
//    }
//}
