package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Calendar;
import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.NearDayStatisticsResponse;
import com.wone.woneprojectapi.model.calendar.*;
import com.wone.woneprojectapi.model.challenge.ChallengeCreateRequest;
import com.wone.woneprojectapi.repository.CalendarRepository;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CalendarService {
    private final CalendarRepository calendarRepository;


    /**
     * 소비 내역 등록
     * @param member
     * @param challenge
     * @param request
     */
    public void setCalendar(Member member, Challenge challenge, CalendarCreateRequest request){
        Calendar addDate = new Calendar();
        addDate.setMember(member);
        addDate.setSpendDate(LocalDate.now());
        addDate.setSpendTime(request.getSpendTime());
        addDate.setCategoryType(request.getCategoryType());
        addDate.setAmount(request.getAmount());
        addDate.setChallenge(challenge);

        calendarRepository.save(addDate);
    }


    /**
     * 사용자별 소비내역 조회(전체)
     * @param member
     * @return
     */
    public List<CalendarUserItem> getCalendarsUser(Member member){
        List<Calendar> originList = calendarRepository.findAllByMemberOrderByIdDesc(member);

        List<CalendarUserItem> result = new LinkedList<>();

        for(Calendar calendar: originList){
            CalendarUserItem addItem = new CalendarUserItem();
            addItem.setId(calendar.getId());
            addItem.setMemberId(calendar.getMember().getId());
            addItem.setSpendDate(calendar.getSpendDate());
            addItem.setSpendTime(calendar.getSpendTime());
            addItem.setCategoryImgUrl(calendar.getCategoryType().getCategoryImgUrl());
            addItem.setCategoryName(calendar.getCategoryType().getCategoryName());
            addItem.setAmount(calendar.getAmount());

            result.add(addItem);
        }
        return result;

    }

    // 오늘 나의 지출 카테고리별 금액 구하기
    // 멤버별 총 하루 사용 금액
//    public CalendarStaticResponse getStatics(Member member, Challenge challenge) throws Exception{
//        LocalDate today = LocalDate.now(); // 오늘 구하기
//        // "나"의 "오늘" 지출 내역들 전체 가져오기
//        List<Calendar> calendars = calendarRepository.findAllByMemberAndSpendDate(member, today);
//
//        double totalAmount = 0D;
//
//
//        for (Calendar calendar : calendars) {
//            totalAmount += calendar.getAmount();
//        }
//
//        CalendarStaticResponse response = new CalendarStaticResponse();
//        response.setTotalAmount(totalAmount);
//        response.setGoalsMoney(challenge.getGoalsMoney());
//
//        double todayTotalMoney = challenge.getGoalsMoney() - totalAmount;
//        response.setTodayTotalMoney(todayTotalMoney);
//
//        return response;
//    }

//    // 하루 지출 금액 불러오기
//    public NearDayStatisticsResponse getNearDayStatistics(Member member, LocalDate spendDate) {
//        LocalDateTime today = LocalDateTime.now();
//
//        NearDayStatisticsResponse response = new NearDayStatisticsResponse();
//
//        List<Calendar> spendDatasets = calendarRepository.findAllByMemberAndSpendDate(member, spendDate);
//
//
//        // 데이터를 2024-02-17T11:45:50.605258718 형식 말고 잘라서 줘야 함.
//        String LocalDateForm = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//
//        LocalDateTime startTime = LocalDateTime.of(
//                today.getYear(),
//                today.getMonth(),
//                today.getDayOfMonth(),
//                0,
//                0,
//                0
//        );
//        LocalDateTime endTime = LocalDateTime.of(
//                today.getYear(),
//                today.getMonth(),
//                today.getDayOfMonth(),
//                23,
//                59,
//                59
//        );
//        datasets.add();
//    }


    /**
     * 하루 총 지출 내역 합계 계산
     * @param member
     */
    public CalendarTodayStaticResponse getTodayStatics(Member member) {
        LocalDate today = LocalDate.now(); // 오늘 구하기
        // "나"의 "오늘" 지출 내역들 전체 가져오기
        List<Calendar> calendars = calendarRepository.findAllByMemberAndSpendDate(member, today);

        double totalAmount = 0D;


        for (Calendar calendar : calendars) {
            totalAmount += calendar.getAmount();
        }

        CalendarTodayStaticResponse response = new CalendarTodayStaticResponse();
        response.setTotalAmount(totalAmount);

        return response;
    }


    /**
     * 지출 내역 계산
     * @param member
     * @param challenge
     * @return
     * @throws Exception
     */
    public CalendarStaticResponse getStatics(Member member, Challenge challenge) throws Exception{
        LocalDate today = LocalDate.now(); // 오늘 구하기
        // "나"의 "오늘" 지출 내역들 전체 가져오기
        List<Calendar> calendars = calendarRepository.findAllByMemberAndSpendDateAndChallenge(member, today, challenge);

        double totalAmount = 0D;

        for (Calendar calendar : calendars) {
            totalAmount += calendar.getAmount();
        }

        CalendarStaticResponse response = new CalendarStaticResponse();
        response.setTotalAmount(totalAmount);
        response.setGoalsMoney(challenge.getGoalsMoney());

        double todayTotalMoney = challenge.getGoalsMoney() - totalAmount;
        response.setTodayTotalMoney(todayTotalMoney);

        return response;
    }

    /**
     * 목표 금액 - 지출 내역 계산
     * @param member
     * @param challenge
     * @return
     * @throws Exception
     */
    public CalendarStaticResponse getStaticsTotal(Member member, Challenge challenge) throws Exception{
        LocalDate today = LocalDate.now(); // 오늘 구하기
        // "나"의 "오늘" 지출 내역들 전체 가져오기
        List<Calendar> calendars = calendarRepository.findAllByMemberAndSpendDate(member, today);

        double totalAmount = 0D;

        for (Calendar calendar : calendars) {
            totalAmount += calendar.getAmount();
        }

        CalendarStaticResponse response = new CalendarStaticResponse();
        response.setTotalAmount(totalAmount);
        response.setGoalsMoney(challenge.getGoalsMoney());

        double todayTotalMoney = challenge.getGoalsMoney() - totalAmount;
        response.setTodayTotalMoney(todayTotalMoney);

        return response;
    }


    /**
     * 지출 내역 상세 조회
     * @param id
     * @return
     */
    public CalendarResponse getCalendar(long id){
        Calendar originData = calendarRepository.findById(id).orElseThrow();
        CalendarResponse response = new CalendarResponse();
        response.setMemberId(originData.getMember().getId());
        response.setSpendDate(originData.getSpendDate());
        response.setSpendTime(originData.getSpendTime());
        response.setCategoryImgUrl(originData.getCategoryType().getCategoryImgUrl());
        response.setCategoryName(originData.getCategoryType().getCategoryName());
        response.setAmount(originData.getAmount());

        return response;
    }


    /**
     * 지출 내역 수정
     * @param id
     * @param request
     */
    public void putSpending(long id, CalendarSpendingChangeRequest request){
        Calendar originData = calendarRepository.findById(id).orElseThrow();
        originData.setMember(originData.getMember());
        originData.setSpendTime(originData.getSpendTime());
        originData.setCategoryType(originData.getCategoryType());
        originData.setAmount(request.getAmount());
        calendarRepository.save(originData);
    }

    /**
     * 지출내역 삭제
     * @param id
     */
    public void delSpending(long id){
        calendarRepository.deleteById(id);
    }
}
