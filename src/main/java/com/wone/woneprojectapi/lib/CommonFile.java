package com.wone.woneprojectapi.lib;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class CommonFile {
    // static은 heap 영역에 들어가지 않고 상수를 저장하는 영역으로 들어간다
    // 모든 걸 static으로 처리할 경우 메모리가 다 차서 위험
    // java.io.tmpdir = project /
    public static File multipartToFile (MultipartFile multipartFile) throws IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + multipartFile.getOriginalFilename());
        // multipartFile을 변환해서 convFile에 넣으세요
        multipartFile.transferTo(convFile);
        return convFile;
    }
}
