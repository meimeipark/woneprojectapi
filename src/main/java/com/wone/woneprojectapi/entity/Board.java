package com.wone.woneprojectapi.entity;

import com.wone.woneprojectapi.enums.BoardType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // LAZY로 변경 필요
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "memberId", nullable = false)
//    private Member member;


    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private BoardType boardType;

    @Column(nullable = false)
    private LocalDateTime boardCreateDate;

    @Column(nullable = false, length = 40)
    private String boardTitle;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String boardContent;
}
