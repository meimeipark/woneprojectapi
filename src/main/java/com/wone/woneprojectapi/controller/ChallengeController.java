package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.challenge.ChallengeCreateRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeGoalsMoneyChangeRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeItem;
import com.wone.woneprojectapi.model.challenge.ChallengeResponse;
import com.wone.woneprojectapi.service.ChallengeService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/challenge")
public class ChallengeController {
    private final MemberService memberService;
    private final ChallengeService challengeService;

    @PostMapping("/member-id/{memberId}")
    @Operation(summary = "목표금액 등록")
    public CommonResult setChallenge(@PathVariable long memberId, @RequestBody ChallengeCreateRequest request){
        Member member = memberService.getData(memberId);
        challengeService.setChallenge(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/goals-money/all/member-id/{memberId}")
    @Operation(summary = "목표금액 멤버별 조회")
    public ListResult<ChallengeItem> getChallengeMember(@PathVariable long memberId){
        Member member = memberService.getData(memberId);
        return ResponseService.getListResult(challengeService.getChallengeMember(member));

    }

    @GetMapping("/challenge-goals-money/today/detail/{memberId}")
    @Operation(summary = "멤버별 금일 목표금액 조회")
    public SingleResult<ChallengeResponse> getTodayChallenge(@PathVariable long memberId){
        Member member = memberService.getData(memberId);
        return ResponseService.getSingleResult(challengeService.getTodayChallenge(member));
    }

    @GetMapping("/challenge-goals-money/detail/{id}")
    @Operation(summary = "목표금액 개별 조회")
    public SingleResult<ChallengeResponse> getChallenge(@PathVariable long id){
        return ResponseService.getSingleResult(challengeService.getChallenge(id));
    }

    @PutMapping("/challenge-goals-money/{id}")
    @Operation(summary = "목표 금액 수정")
    public CommonResult putChallenge(@PathVariable long id, @RequestBody ChallengeGoalsMoneyChangeRequest request){
        challengeService.putChallenge(id, request);

        return ResponseService.getSuccessResult();
    }
}
