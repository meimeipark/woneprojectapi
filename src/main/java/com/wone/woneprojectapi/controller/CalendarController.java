package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.NearDayStatisticsResponse;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.calendar.*;
import com.wone.woneprojectapi.service.CalendarService;
import com.wone.woneprojectapi.service.ChallengeService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calendar")
public class CalendarController {
    private final CalendarService calendarService;
    private final MemberService memberService;
    private final ChallengeService challengeService;

    @PostMapping("/member-id/{memberId}/challenge-id/{challengeId}")
    @Operation(summary = "소비 내역 등록")
    public CommonResult setCalendar(@PathVariable long memberId, @PathVariable long challengeId ,@RequestBody CalendarCreateRequest request){
        Member member = memberService.getData(memberId);
        Challenge challenge = challengeService.getDataMoney(challengeId);
        calendarService.setCalendar(member, challenge,request);
        return ResponseService.getSuccessResult();
    }


    @GetMapping("/one-day-spending/member-id/{memberId}")
    @Operation(summary = "사용자별 소비내역 조회(전체)")
    public ListResult<CalendarUserItem> getCalendarsUser(@PathVariable long memberId){
        Member member = memberService.getData(memberId);

        return ResponseService.getListResult(calendarService.getCalendarsUser(member));
    }


    @GetMapping("/one-day-spending/{id}")
    @Operation(summary = "지출 내역 상세 조회")
    public SingleResult<CalendarResponse> getCalender(@PathVariable long id){
        return ResponseService.getSingleResult(calendarService.getCalendar(id));
    }

    @GetMapping("/statics/member-id/{memberId}/challenge-id/{challengeId}")
    @Operation(summary = "지출 내역 계산")
    public SingleResult<CalendarStaticResponse> getStatics(@PathVariable long memberId, @PathVariable long challengeId) throws Exception{
        Member member = memberService.getData(memberId);
        Challenge challenge = challengeService.getDataMoney(challengeId);
        return ResponseService.getSingleResult(calendarService.getStatics(member, challenge));
    }

    @GetMapping("/statics/today/member-id/{memberId}")
    @Operation(summary = "하루 총 지출 내역 합계 계산")
    public SingleResult<CalendarTodayStaticResponse> getTodayStatics(@PathVariable long memberId) throws Exception{
        Member member = memberService.getData(memberId);
        return ResponseService.getSingleResult(calendarService.getTodayStatics(member));
    }




    @PutMapping("/spending-change/{id}")
    @Operation(summary = "지출 내역 수정")
    public CommonResult putSpending(@PathVariable long id, CalendarSpendingChangeRequest request){
        calendarService.putSpending(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/spending/{id}")
    @Operation(summary = "지출 내역 삭제")
    public CommonResult delSpending(@PathVariable long id){
        calendarService.delSpending(id);

        return ResponseService.getSuccessResult();
    }
}
