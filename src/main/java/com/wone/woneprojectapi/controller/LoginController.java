package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.model.LoginRequest;
import com.wone.woneprojectapi.model.LoginResponse;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.service.LoginService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/member")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest request) {
        return loginService.doLogin(request);
    }
}
