package com.wone.woneprojectapi.controller;

import com.wone.woneprojectapi.enums.BoardType;
import com.wone.woneprojectapi.model.CommonResult;
import com.wone.woneprojectapi.model.ListResult;
import com.wone.woneprojectapi.model.SingleResult;
import com.wone.woneprojectapi.model.board.notice.*;
import com.wone.woneprojectapi.service.BoardService;
import com.wone.woneprojectapi.service.MemberService;
import com.wone.woneprojectapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;

//    @PostMapping("/new/member-id/{memberId}")
//    @Operation(summary = "공지사항, 이용방법 게시판 등록")
//    public CommonResult setBoard (@PathVariable long memberId, @RequestBody BoardCreateRequest request){
//        Member member = memberService.getData(memberId);
//        boardService.setBoard(member, request);
//
//        return ResponseService.getSuccessResult();
//    }
    @PostMapping("/new/notice")
    @Operation(summary = "공지사항 게시판 등록")
    public CommonResult setNoticeBoard (@RequestBody BoardCreateRequest request){
        boardService.setNoticeBoard(request);
        return ResponseService.getSuccessResult();
    }

    @PostMapping("/new/guide")
    @Operation(summary = "이용방법 게시판 등록")
    public CommonResult setGuideBoard (@RequestBody BoardCreateRequest request){
        boardService.setGuideBoard(request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/notice/all")
    @Operation(summary = "보드 타입별로 조회하기")
    public ListResult<BoardNoticeItem> getNotices (@RequestParam(name="boardType")BoardType boardType){
        return ResponseService.getListResult(boardService.getNotices(boardType));
    }

    @GetMapping("/use/all")
    @Operation(summary = "게시판 이용방법만 모두 조회")
    public ListResult<BoardUseItem> getUseBoards (){
        return ResponseService.getListResult(boardService.getUseBoards());
    }

    @GetMapping("/detail/notice/{id}")
    @Operation(summary = "게시판(공지사항) 내용 상세보기")
    public SingleResult<BoardNoticeResponse> getNoticeBoard(@PathVariable long id){
        return ResponseService.getSingleResult(boardService.getNoticeBoard(id));
    }

    @GetMapping("/detail/use/{id}")
    @Operation(summary = "이용방법 상세보기 (등록날짜 제거 ver)")
    public SingleResult<BoardUseResponse> getUesBoard(@PathVariable long id){
        return ResponseService.getSingleResult(boardService.getUseBoard(id));
    }


    @GetMapping("/all/guide/{pageNum}")
    @Operation(summary = "게시판 페이징 성공")
    public ListResult<BoardItem> getGuides(@PathVariable int pageNum, BoardType boardType) {
        return boardService.getGuides(pageNum, boardType);
    }

    @PutMapping("/change-content/{id}")
    @Operation(summary = "게시판 내용 수정하기")
    public CommonResult putBoard(@PathVariable long id, @RequestBody BoardContentChangeRequest request){
        boardService.putBoard(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "게시글 삭제하기")
    public CommonResult delBoard(@PathVariable long id){
        boardService.delBoard(id);

        return ResponseService.getSuccessResult();
    }

}
