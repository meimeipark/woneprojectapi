package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Calendar;
import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CalendarRepository extends JpaRepository<Calendar, Long> {
    List<Calendar> findAllByMemberOrderByIdDesc(Member member);
//    List<Calendar> findAllByMemberOrderBySpendDateDesc(Member member);

//    List<Calendar> findAllByMemberSpendDateAndMoneyTypeOrderByDesc(Member member, LocalDate spendDate, MoneyType moneyType);

    List<Calendar> findAllByMemberAndSpendDate(Member member, LocalDate spendDate);
    List<Calendar> findAllByMemberAndSpendDateAndChallenge(Member member, LocalDate spendDate, Challenge challenge);


}
