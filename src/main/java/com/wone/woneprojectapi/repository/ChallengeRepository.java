package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ChallengeRepository extends JpaRepository<Challenge, Long> {

    List<Challenge> findAllByMemberOrderByChallengeDateDesc(Member member);

    Optional<Challenge> findByMemberAndChallengeDate(Member member, LocalDate challengeDate);
}
