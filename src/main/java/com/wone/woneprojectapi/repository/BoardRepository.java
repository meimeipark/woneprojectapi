package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Board;
import com.wone.woneprojectapi.enums.BoardType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

public interface BoardRepository extends JpaRepository<Board, Long> {
    List<Board> findAllByBoardTypeEqualsOrderByIdDesc(BoardType boardType);

    /** 보드 타입별로 조회하기 **/
    List<Board> findAllByBoardTypeOrderByIdDesc(BoardType boardType);

    /** 보드 타입별 페이징 **/
    Page<Board> findALLByBoardTypeOrderByIdDesc(PageRequest pageRequest, BoardType boardType);

    Optional<Board> findAllByBoardType(BoardType boardType);
}