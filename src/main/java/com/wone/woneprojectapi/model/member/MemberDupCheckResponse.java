package com.wone.woneprojectapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

// 중복 확인 체크
public class MemberDupCheckResponse {
    private Boolean isNew;
}
