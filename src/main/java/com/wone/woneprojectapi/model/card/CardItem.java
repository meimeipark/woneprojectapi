package com.wone.woneprojectapi.model.card;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CardItem {
    private Long id;
    private Long memberId;
    private String cardGroup;
    private String cardNumber;
    private LocalDate endDate;
    private Short cvc;
    private String etcMemo;
}
