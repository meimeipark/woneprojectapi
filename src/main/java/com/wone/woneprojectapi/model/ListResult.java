package com.wone.woneprojectapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private String msg;
    private Integer code;
    private List<T> list;
    private Long totalCount; // 총 데이터 갯수
    private Integer totalPage; // 총 페이지 수 --> 1 (한 뭉텡이씩 주니까)
    private Integer currentPage; // 현재 페이지 번호 --> 1
}
