package com.wone.woneprojectapi.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class LoginRequest {
    @NonNull
    @Length(min = 5, max = 15)
    private String userId;

    @NonNull
    @Length(min = 7, max = 20)
    private String password;
}
