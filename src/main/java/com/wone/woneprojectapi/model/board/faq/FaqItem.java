package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class FaqItem {
    private Long id;
    private String memberName;
    private String askContent;
    private String publicType;
}
