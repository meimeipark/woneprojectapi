package com.wone.woneprojectapi.model.board.notice;

import com.wone.woneprojectapi.enums.BoardType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardCreateRequest {
//    @Enumerated(value = EnumType.STRING)
//    private BoardType boardType;
    private LocalDate boardCreateData;
    private String boardTitle;
    private String boardContent;
}
