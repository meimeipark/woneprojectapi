package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FaqAdminChangeRequest {
    private String comment;
}
