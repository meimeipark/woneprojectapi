package com.wone.woneprojectapi.model.board.notice;

import com.wone.woneprojectapi.enums.BoardType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardContentChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private BoardType boardType;
    private String boardTitle;
    private String boardContent;
}
