package com.wone.woneprojectapi.model.board.notice;

import com.wone.woneprojectapi.enums.BoardType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardUseResponse {
    private Long id;
    private BoardType boardType;
    private String boardTitle;
    private String boardContent;
}
