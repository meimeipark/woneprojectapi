package com.wone.woneprojectapi.model.board.faq;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class FaqCreateRequest {
//    private LocalDateTime askCreateDate;
    private String askContent;
    private Boolean publicType;
    private Short askPassword;
}
