package com.wone.woneprojectapi.model.calendar;

import com.wone.woneprojectapi.enums.CategoryType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class CalendarSpendingChangeRequest {

    private String spendTime;

    @Enumerated(value = EnumType.STRING)
    private CategoryType categoryType;
    private Double amount;
}
