package com.wone.woneprojectapi.model.calendar;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalendarTodayStaticResponse {
    private Double totalAmount;
}
