package com.wone.woneprojectapi.model.calendar;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CalendarStaticResponse {
    private Double goalsMoney;
    private Double totalAmount;
    private Double todayTotalMoney;
}
